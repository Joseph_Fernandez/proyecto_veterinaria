from django.db import models

# Create your models here.
class Mascota(models.Model):
    nroregistro=models.CharField(max_length=3)
    animal=models.CharField(max_length=10)
    raza=models.CharField(max_length=15)
    genero=models.CharField(max_length=8)
    peso=models.IntegerField()
    edad=models.CharField(max_length=15)
   

class Persona_adopta(models.Model):

    rut=models.CharField(max_length=20)
    nombre=models.CharField(max_length=30)
    direccion=models.CharField(max_length=40)
    telefono=models.CharField(max_length=15)
    correo=models.EmailField()
    fecnac=models.DateField()
    preferencia=models.CharField(max_length=10)

