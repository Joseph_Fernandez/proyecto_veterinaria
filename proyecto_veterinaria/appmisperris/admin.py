from django.contrib import admin

# Register your models here.
from appmisperris.models import Mascota,Persona_adopta

admin.site.register(Mascota)
admin.site.register(Persona_adopta)

