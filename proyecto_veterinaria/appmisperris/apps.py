from django.apps import AppConfig


class AppmisperrisConfig(AppConfig):
    name = 'appmisperris'
