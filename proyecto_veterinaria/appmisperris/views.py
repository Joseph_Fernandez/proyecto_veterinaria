from django.shortcuts import render
from django.http import HttpResponse
from appmisperris.models import Mascota,Persona_adopta



# Create your views here.

#Formulario de ingreso de animal
def formulario_ingreso_animal(request):
    return render(request,"ingresar_animal.html")

def formulario_ingreso_adopcion(request):
    return render(request,"ingresar_persona_adoptiva.html")

def busqueda_preferencia(request):
    return render(request,"ingresar_persona_adoptiva.html")

def ingresar_animal(request):
    nroregistro=request.GET["txt_nroregistro"]
    animal=request.GET["txt_animal"]
    raza=request.GET["txt_raza"]
    genero=request.GET["txt_genero"]
    peso=request.GET["txt_peso"]
    edad=request.GET["txt_edad"]
    if len(nroregistro)>0 and len(animal)>0 and len(raza)>0 and len(genero)>0 and len(peso)>0 and len(edad)>0:
        masc=Mascota(nroregistro=nroregistro,animal=animal,raza=raza,genero=genero,peso=peso,edad=edad)
        masc.save()
        mensaje="Animalito registrado!"
    else:
        mensaje="Animalito no registrado. Faltan datos por ingresar..."
    return HttpResponse(mensaje)

def ingresar_persona_adoptiva(request):
    rut=request.GET["txt_rut"]
    nombre=request.GET["txt_nombre"]
    direccion=request.GET["txt_direccion"]
    telefono=request.GET["txt_telefono"]
    correo=request.GET["txt_correo"]
    fecnac=request.GET["txt_fecnac"]
    preferencia=request.GET["txt_preferencia"]
    if len(rut)>0 and len(nombre)>0 and len(direccion)>0 and len(telefono)>0 and len(correo)>0 and len(fecnac)>0 and len(preferencia)>0:
        per=Persona_adopta(rut=rut,nombre=nombre,direccion=direccion,telefono=telefono,correo=correo,fecnac=fecnac,preferencia=preferencia)
        per.save()
        mensaje="Solicitud de adopcion registrada con exito!Nos contactaremos a la brevedad!"
    else:
        mensaje="Solicitud no registrada.Faltan datos por ingresar..."
    return HttpResponse(mensaje)        

def buscar(request):
    if request.GET["txt_pref"]:
        animal_recibido = request.GET["txt_pref"]
        #no tomar en cuenta este error ya que el profe dijo que no era un error en si ojo.
        mascota=Mascota.objects.filter(animal__contains=animal_recibido)
        return render(request,"resultado_busqueda.html",{"mascota":mascota,"mascota_consultada":animal_recibido})
    else: 
        mensaje="Debe ingresar un animal a buscar"
    return HttpResponse(mensaje)

